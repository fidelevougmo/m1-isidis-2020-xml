async function sayHello() {
    
    const xml = await get('/xml/countries.xml');
    const country = new URL(location).searchParams.get('country');

    let nodes = xml.evaluate("/countries/country[name='" + country + "']/name",  xml, null, XPathResult.ANY_TYPE, null);
    var name = nodes.iterateNext();

    cc = name.getAttribute('cc');
    
    const hello = await get("https://fourtonfish.com/hellosalut/?cc=" + cc, mode='json');

    res = JSON.parse(hello);

    document.querySelector('#say_hello').innerHTML = res.hello;
}

sayHello();
