async function display() {

    const country = new URL(location).searchParams.get('country');

    const xml = await get('/xml/countries.xml');
    const xsl = await get('/xsl/country_infos.xsl');

    const proc = new XSLTProcessor();
    proc.importStylesheet(xsl);
    proc.setParameter(null, 'country', country);
    const result = proc.transformToFragment(xml, document);

    document.querySelector('#frag_country_infos').appendChild(result);
}

display();
