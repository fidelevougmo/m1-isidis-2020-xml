let name = '';
let continent = '';

async function display() {
    // TODO: load xml and xsl
    const xml = await get('/xml/countries.xml');
    const xsl = await get('/xsl/country_list.xsl');

    // TODO: xsl transformations
    const proc = new XSLTProcessor();
    proc.importStylesheet(xsl);
    proc.setParameter(null, 'name', name);
    proc.setParameter(null, 'continent', continent);
    result = proc.transformToFragment(xml, document);

    document.querySelector('#frag_country_list').innerHTML = '';

    // TODO: append in #frag_country_list
    document.querySelector('#frag_country_list').appendChild(result);
}

search_name.onkeyup = function() {
    name = this.value;
    display();
}

search_continent.onchange = function() {
    continent = this.value;
    display();
}

display();
