<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="country"/>

<xsl:template match="/">
    <xsl:apply-templates select="countries/country[name=$country]" />
</xsl:template>

<xsl:template match="country">

    <h1>
        <xsl:value-of select="name"/>
        <span class="badge badge-light" style="text-transform: uppercase;">
            <strong><xsl:value-of select="name/@cc"/></strong>
        </span>
    </h1>

    <hr/>

    <div class="row">
        <div class="col-4">
            <img src="{flag/@src}" width="100%"/>
        </div>

        <div class="col-8">
            <p><strong>Capital city: </strong> <xsl:value-of select="capital"/></p>
            <p><strong>Population: </strong> <xsl:value-of select="population"/> inhabitants</p>
            <p><strong>Area: </strong> <xsl:value-of select="area"/> km<sup>2</sup></p>
        </div>
    </div>

    <hr/>

    <h1>Location on Earth</h1>

    <iframe width="100%"
            height="400"
            frameborder="0"
            marginheight="0"
            marginwidth="0"
            src="https://maps.google.com/maps?q={name}&amp;hl=en&amp;output=embed">
    </iframe>

</xsl:template>

</xsl:stylesheet>
