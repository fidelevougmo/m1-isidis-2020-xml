<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="name"/>
<xsl:param name="continent"/>

<xsl:variable name="country_filter"
              select="/countries/country[
                        contains(name, $name)
                        and contains(@continent, $continent)]" />

<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="$country_filter"> <!--.//country-->
        <xsl:sort select="name"/>
        <tr>
          <td><img src="{flag/@src}" height="30px" width="50px"/></td>
          <td><a href="country_infos.html?country={name}"><xsl:value-of select="name"/></a></td>
          <td><xsl:value-of select="@continent"/></td>
        </tr>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>

</xsl:stylesheet>
